"use strict"

module.exports = {
  environment: process.env.NODE_ENV || "development",
  port: process.env.PORT || 3000,
  vkApiAccessToken: process.env.VK_API_ACCESS_TOKEN
}