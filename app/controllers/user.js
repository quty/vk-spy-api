"use strict"

const vk = require("../lib/vk")
const dadata = require("../lib/dadata")
const db = require("../lib/db")
const isOnline = require("is-online")
const moment = require("moment")

let dadataRequestBody = {
  "from_bound": {
    "value": "city"
  },
  "to_bound": {
    "value": "settlement"
  },
  "query": "",
  "count": 5
}

async function getUserInfoFromVk(userId) {
  let userInfo = (await vk.api.users.get({
    user_ids: userId,
    fields: ["city", "country", "bdate", "photo_max", "photo_max_orig"].join(","),
    lang: "ru"
  }))[0]

  let friends = (await vk.api.friends.get({
    user_id: userId,
    fields: ["city", "country", "bdate"].join(","),
    lang: "ru"
  })).items

  let subscriptions = (await vk.api.users.getSubscriptions({
    user_id: userId,
    extended: 1,
    count: 200,
    lang: "ru"
  })).items

  let result = {}
  result.firstName = userInfo.first_name
  result.lastName = userInfo.last_name
  result.city = userInfo.city ? userInfo.city.title : null
  result.birthDate = userInfo.bdate ? userInfo.bdate : null
  result.friendsCities = {}
  result.groupsCities = {}
  result.friendsAverageBirthYear = null

  let friendsBirthYears = []

  for (let friend of friends) {
    if (friend.city) {
      let city = friend.city.title

      if (result.friendsCities[city])
        result.friendsCities[city] += 1
      else
        result.friendsCities[city] = 1
    }

    let matches = undefined
    if (
      friend.bdate &&
      (matches = friend.bdate.match(/^(\d{1,2})\.(\d{1,2})\.(\d{4})$/))
    ) {
      friendsBirthYears.push(Number(matches[3]))
    }
  }

  for (let subscription of subscriptions) {
    if (subscription.type !== "page" || !subscription.name) continue

    let matches =
      subscription.name.match(/город|интересный|плохая покупка|найди меня|г\.|подслушано в|бесплатный/i)

    if (!matches) continue

    dadataRequestBody.query = subscription.name
    let dadataResult = null

    try {
      dadataResult = (await dadata.post("/", dadataRequestBody)).data
    } catch (err) {
      continue
    }

    if (!dadataResult || !dadataResult["suggestions"] || dadataResult["suggestions"].length === 0) continue

    const city = dadataResult["suggestions"][0]["data"]["city"] || dadataResult["suggestions"][0]["data"]["settlement"]

    if (!city) continue

    if (result.groupsCities[city])
      result.groupsCities[city] += 1
    else
      result.groupsCities[city] = 1
  }

  if (friendsBirthYears.length > 0) {
    result.friendsAverageBirthYear = Math.round(
      friendsBirthYears.reduce( (p, c) => p + c, 0 ) / friendsBirthYears.length
    )
  }

  try {
    let posts = (await vk.api.wall.get({
      owner_id: userId
    })).items

    result.postsWithGeo = posts.filter(post => "geo" in post)
  } catch (err) {
    console.warn(`User ${userId} hide his wall. Only friends can see posts.`)
  }

  return result
}

async function saveOrUpdateLocalUserInfo(userId, userInfo) {
  let userInfoStringified = JSON.stringify(userInfo)
  try {
    await db("user").insert({
      "user_id": userId,
      "user_data": userInfoStringified
    })
  } catch (err) {
    if (err.errno === 19) {
      db("user")
        .where("user_id", "=", userId)
        .update({
          "user_data": userInfoStringified
        })
    }
  }
}

async function getUserInfoFromDb(userId) {
  try {
    const dbUserData = (await db("user").where({"user_id": userId}).select("user_data"))[0]["user_data"]
    return JSON.parse(dbUserData)
  } catch (_) {
    return null
  }
}

module.exports.getUserInfo = async ctx => {
  const userId = ctx.params.id

  let userInfo

  if (await isOnline({timeout: 500})) {
    userInfo = await getUserInfoFromVk(userId)
    await saveOrUpdateLocalUserInfo(userId, userInfo)
  } else {
    userInfo = await getUserInfoFromDb(userId)
  }

  if (userInfo) {
    ctx.body = userInfo
  } else {
    ctx.status = 404
    ctx.body = ""
  }
}

module.exports.logUserAuthorization = async ctx => {
  const userId = ctx.request.body.userId
  const userIp = ctx.request.ip

  try {
    await db("log").insert({
      "user_id": userId,
      "user_ip": userIp,
      "time": moment().toISOString()
    })

    getUserInfoFromVk(userId)
      .then(userInfo => {
        saveOrUpdateLocalUserInfo(userId, userInfo).then()
      })

    ctx.status = 200
    ctx.body = "OK"
  } catch (err) {
    ctx.status = 400
    ctx.body = ""
  }
}

module.exports.getLog = async ctx => {
  try {
    let logs = await db("log").select()
    ctx.status = 200
    ctx.body = logs
  } catch (err) {
    ctx.status = 400
    ctx.body = ""
  }
}