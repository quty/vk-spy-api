const Koa  = require("koa")
const bodyParser = require("koa-bodyparser")
const router = require("./routes")
const cors = require("@koa/cors")
const db = require("./lib/db")
const config = require("./config")

const app = new Koa()

app.use(bodyParser())
app.use(cors())
app.use(router.routes())

app.listen(config.port, () => {
  console.log(`Server listening on ${config.port}`)
  db.migrate.latest()
    .then()
    .catch(err => console.error(err))
})