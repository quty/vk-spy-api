"use strict"

const Axios = require("axios")

const dadata = Axios.create({
  baseURL: "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address/",
  headers: {
    "Authorization": "Token 5ef98f5781a106962077fb18109095f9f11ebac1",
    "Origin": "https://s.codepen.io",
    "Content-Type": "application/json",
    "Accept": "application/json, text/javascript, */*; q=0.01",
    "Referer": "https://s.codepen.io/boomerang/iFrameKey-bf4837cc-dbda-687e-146c-8cc155a66569/index.html?editors=1010"
  }
})

module.exports = dadata