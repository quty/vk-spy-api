"use strict"

const { environment } = require("../config")
module.exports = require("knex")(require("../../knexfile")[environment])