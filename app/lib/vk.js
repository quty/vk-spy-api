"use strict"

const { vkApiAccessToken } = require("../config")

const { VK } = require("vk-io")

module.exports = new VK({
  token: vkApiAccessToken
})