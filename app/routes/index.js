"use strict"

const Router = require("koa-router")
const userRouter = require("./user")
const logRouter = require("./log")

const router = new Router({
  prefix: "/api"
})

router.use(userRouter.routes())
router.use(logRouter.routes())

module.exports = router