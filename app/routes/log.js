"use strict"

const Router = require("koa-router")
const userController = require("../controllers/user")

const router = new Router({
  prefix: "/log"
})

router.get("/", userController.getLog)
router.post("/", userController.logUserAuthorization)

module.exports = router