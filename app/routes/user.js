"use strict"

const Router = require("koa-router")
const userController = require("../controllers/user")

const router = new Router({
  prefix: "/users"
})

router.get("/:id", userController.getUserInfo)

module.exports = router