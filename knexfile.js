"use strict"

module.exports = {
  development: {
    client: "sqlite3",
    connection: {
      filename: "./development.sqlite"
    },
    useNullAsDefault: true
  }
}