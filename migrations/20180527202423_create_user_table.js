let table = (table) => {
  table.integer("user_id").primary()
  table.binary("user_data")
}

exports.up = function(knex, Promise) {
  return knex.schema.createTable("user", table)
    .then(() => {
      console.log("Successfully created 'users' table")
    })
    .catch((err) => {
      console.log(`Failed to create 'users' table with error ${err}`)
    })
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTable("user", table)
    .then(() => {
      console.log("Successfully deleted 'users' table")
    })
    .catch((err) => {
      console.log(`Failed to delete 'users' table with error ${err}`)
    })
}
