let table = (table) => {
  table.increments()
  table.integer("user_id")
  table.string("user_ip")
  table.datetime("time")
}

exports.up = function(knex, Promise) {
  return knex.schema.createTable("log", table)
    .then(() => {
      console.log("Successfully created 'log' table")
    })
    .catch((err) => {
      console.log(`Failed to create 'log' table with error ${err}`)
    })
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTable("log", table)
    .then(() => {
      console.log("Successfully deleted 'log' table")
    })
    .catch((err) => {
      console.log(`Failed to delete 'log' table with error ${err}`)
    })
}
